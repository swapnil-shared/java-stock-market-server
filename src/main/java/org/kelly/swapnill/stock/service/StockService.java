package org.kelly.swapnill.stock.service;

import org.kelly.swapnill.stock.model.StockUI;
import org.springframework.http.codec.ServerSentEvent;
import reactor.core.publisher.Flux;

import java.util.Collection;

public interface StockService {

    /**
     * Method to get latest stock updates
     *
     * @return {@code Flux<ServerSentEvent<Collection<StockUI>>>} Latest stock details information
     */
    Flux<ServerSentEvent<Collection<StockUI>>> getLiveStocks();

}
