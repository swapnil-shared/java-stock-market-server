package org.kelly.swapnill.stock.service.impl;

import org.kelly.swapnill.stock.model.StockUI;
import org.kelly.swapnill.stock.service.ExternalApiService;
import org.kelly.swapnill.stock.service.StockService;
import org.kelly.swapnill.stock.util.constants.Constants;
import org.kelly.swapnill.stock.util.constants.enums.Trend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Stock service implementation
 */
@Service
public class StockServiceImpl implements StockService {

    private static final Map<String, StockUI> STOCKS_DATABASE = new ConcurrentHashMap<>();

    @Autowired
    private ExternalApiService externalApiService;

    @Override
    public Flux<ServerSentEvent<Collection<StockUI>>> getLiveStocks() {
        return Flux.interval(Duration.ofSeconds(5))
                .map(tick -> {
                    this.updateStocks();
                    return STOCKS_DATABASE.values();
                })
                .map(stock -> ServerSentEvent.<Collection<StockUI>>builder()
                        .event(Constants.EVENT_NAME_STOCK_CHANGED)
                        .data(stock)
                        .build());
    }

    private void updateStocks() {
        this.externalApiService.getStockUpdates()
                .parallelStream()
                .forEach(stock -> STOCKS_DATABASE.compute(stock.getSymbol(), (symbol, stockUI) -> {
                    BigDecimal latestMarketPrice = stock.getAsk().add(stock.getBid()).divide(new BigDecimal(2));

                    if (Objects.isNull(stockUI)) {//add stock into database if not present
                        stockUI = new StockUI(
                                stock.getSymbol(),
                                latestMarketPrice,
                                Trend.INITIAL,
                                stock.getLastUpdated());
                    } else if (stock.getLastUpdated() > stockUI.getLastUpdated()) {// if stock is already present in database & latest update is available update it
                        int trend = latestMarketPrice.compareTo(stockUI.getMarketPrice());
                        stockUI.setTrend(
                                trend == 0 ? Trend.NO_CHANGE : trend < 0 ? Trend.DOWN : Trend.UP
                        );
                        stockUI.setMarketPrice(latestMarketPrice);
                    }
                    return stockUI;
                }));
    }
}
