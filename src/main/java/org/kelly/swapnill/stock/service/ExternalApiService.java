package org.kelly.swapnill.stock.service;

import org.kelly.swapnill.stock.model.Stock;

import java.util.List;

public interface ExternalApiService {

    /**
     * Get stock updates from server
     *
     * @return {@code List<Stock>} List of Stock
     */
    List<Stock> getStockUpdates();

}
