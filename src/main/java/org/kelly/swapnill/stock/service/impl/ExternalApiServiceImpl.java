package org.kelly.swapnill.stock.service.impl;

import org.kelly.swapnill.stock.model.Stock;
import org.kelly.swapnill.stock.service.ExternalApiService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Used to interact with external stock server
 */
@Service
public class ExternalApiServiceImpl implements ExternalApiService {

    private final Long APPLICATION_UP_TIME = System.currentTimeMillis();

    @Override
    public List<Stock> getStockUpdates() {
        return Arrays.asList(
                new Stock("A", BigDecimal.ONE, BigDecimal.TEN, APPLICATION_UP_TIME),

                new Stock("B", BigDecimal.ONE, BigDecimal.TEN, System.currentTimeMillis()),

                new Stock("C-1", new BigDecimal(Math.random()), new BigDecimal(Math.random()), System.currentTimeMillis()),
                new Stock("C-2", new BigDecimal(Math.random()), new BigDecimal(Math.random()), System.currentTimeMillis()),
                new Stock("C-3", new BigDecimal(Math.random()), new BigDecimal(Math.random()), System.currentTimeMillis()),

                new Stock(String.format("D-%f", Math.random()), BigDecimal.ONE, BigDecimal.TEN, System.currentTimeMillis())
        );
    }
}
