package org.kelly.swapnill.stock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.kelly.swapnill.stock.util.constants.enums.Trend;

import java.math.BigDecimal;

/**
 * Used for sending response to client
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockUI {

    private String symbol;

    private BigDecimal marketPrice;

    private Trend trend;

    private Long lastUpdated;
}
