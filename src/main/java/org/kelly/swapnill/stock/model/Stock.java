package org.kelly.swapnill.stock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Used for parsing data from stock server
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stock {

    private String symbol;

    private BigDecimal ask;

    private BigDecimal bid;

    private Long lastUpdated;
}
