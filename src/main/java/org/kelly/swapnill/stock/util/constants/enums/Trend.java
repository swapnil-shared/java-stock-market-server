package org.kelly.swapnill.stock.util.constants.enums;

public enum Trend {

    /**
     * When stock is added for the first time and no further updates available
     */
    INITIAL,

    /**
     * When stock is already present and latest update is having same values as last update
     */
    NO_CHANGE,

    /**
     * When trend of pricing going up from last update.
     */
    UP,

    /**
     * When trend of pricing going down from last update.
     */
    DOWN

}
