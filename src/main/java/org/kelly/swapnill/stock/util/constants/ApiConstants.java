package org.kelly.swapnill.stock.util.constants;

/**
 * This class is used to provide API constants for stock-service
 */
public interface ApiConstants {

    String SEPARATOR = "/";
    String STOCK_API = SEPARATOR + "stock";
    String STOCK_LIVE_API = SEPARATOR + "live";

}
