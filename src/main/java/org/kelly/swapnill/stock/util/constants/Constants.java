package org.kelly.swapnill.stock.util.constants;

/**
 * This class is used to provide constants for stock-service
 */
public interface Constants {

    String EVENT_NAME_STOCK_CHANGED = "stock-changed";

}
