package org.kelly.swapnill.stock.controller;

import org.kelly.swapnill.stock.model.StockUI;
import org.kelly.swapnill.stock.service.StockService;
import org.kelly.swapnill.stock.util.constants.ApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.Collection;

@RestController()
@RequestMapping(path = ApiConstants.STOCK_API)
public class StockController {

    @Autowired
    private StockService stockService;

    @GetMapping(path = ApiConstants.STOCK_LIVE_API, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerSentEvent<Collection<StockUI>>> getLiveStocks() {
        return this.stockService.getLiveStocks();
    }

}
