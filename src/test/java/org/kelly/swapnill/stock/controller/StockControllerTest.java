package org.kelly.swapnill.stock.controller;

import org.junit.jupiter.api.Test;
import org.kelly.swapnill.stock.model.StockUI;
import org.kelly.swapnill.stock.util.constants.ApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "80000")
class StockControllerTest {
    @Autowired
    private WebTestClient webTestClient;

    @Test
    void getLiveStocks() {
        final Flux<ServerSentEvent<Collection<StockUI>>> serverSentEventFlux = webTestClient.get()
                .uri(ApiConstants.STOCK_API + ApiConstants.STOCK_LIVE_API)
                .accept(MediaType.TEXT_EVENT_STREAM)
                .exchange()
                .expectStatus().isOk()
                .returnResult(new ParameterizedTypeReference<ServerSentEvent<Collection<StockUI>>>() {
                })
                .getResponseBody();

        StepVerifier.create(serverSentEventFlux)
                .assertNext(collectionServerSentEvent ->
                        assertEquals(6, collectionServerSentEvent.data().size()))
                .assertNext(collectionServerSentEvent ->
                        assertEquals(7, collectionServerSentEvent.data().size()))
                .assertNext(collectionServerSentEvent ->
                        assertEquals(8, collectionServerSentEvent.data().size()))
                .thenCancel()
                .verify();
    }
}