FROM java:8-jdk-alpine
COPY ./build/libs/stock-service-0.0.1-SNAPSHOT.jar usr/app/app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "usr/app/app.jar"]


# docker build -t stock-market:v1 .
# docker run --name stock-market -p 8080:8080 stock-market:v1