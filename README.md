# Read Me First
### Project: Real-time stock pricing display
##### Description:
* This application will show stock name, market price & trend as price changes.
* Developed using Spring boot & ReactJS.
##### Assumptions:
* Reuter API will give us all data.
* How to show trend status is all depends on actual business requirement.
* I have developed this as per my understandings.

# Getting Started

### Repo details

* [Server](https://bitbucket.org/swapnil-shared/java-stock-market-server/src)
* [Client](https://bitbucket.org/swapnil-shared/reactjs-stock-market-client/src)

### How to run
* If you have docker installed you can simply run using below steps
    * Pull image from docker hub
        * docker pull swapnillondhe/public-stock-market:v1
    * Create a container using below command
        * docker run -p 8080:8080 swapnillondhe/public-stock-market:v1
    * Access below url from browser
        * http://localhost:8080/
* Run using gradle
    * Clone the Server repo using above mentioned details
    * Go to project directory and run project using below command
        * ./gradlew bootRun
    * Access below url from browser
        * http://localhost:8080/
* Run using java (uploaded jar into a repository just for easy run)
    * Clone the Server repo using above mentioned details
    * Go to project directory and run jar using below command
        * java -jar stock-market.jar
    * Access below url from browser
        * http://localhost:8080/
